package com.Tsukanov.repositories;

import com.Tsukanov.entities.Email;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRepository extends CrudRepository<Email, Long> {

    @Override
    List<Email> findAll();

}
