package com.Tsukanov.mailclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "com.Tsukanov.entities")
@ComponentScan(basePackages = "com.Tsukanov")
@EnableJpaRepositories(basePackages = "com.Tsukanov")
public class MailClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailClientApplication.class, args);
	}

}

