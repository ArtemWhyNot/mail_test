package com.Tsukanov.entities;

import javax.persistence.*;

@Entity
@Table
public class Email {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String fromUser;
    @Column
    private String toUser;
    @Column
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
