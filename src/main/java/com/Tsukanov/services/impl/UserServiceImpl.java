package com.Tsukanov.services.impl;

import com.Tsukanov.entities.User;
import com.Tsukanov.repositories.UserRepository;
import com.Tsukanov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User loadUser(String username) {
        return userRepository.findByUsername(username);

    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public boolean isAdmin(String username) {
        return userRepository.findByUsername(username).isAdmin();
    }
}
