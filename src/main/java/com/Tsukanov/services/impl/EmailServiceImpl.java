package com.Tsukanov.services.impl;

import com.Tsukanov.entities.Email;
import com.Tsukanov.repositories.EmailRepository;
import com.Tsukanov.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmailServiceImpl implements EmailService {

  @Autowired private EmailRepository emailRepository;

  @Override
  public Email send(Email email) {
    return emailRepository.save(email);
  }

  @Override
  public List<Email> getAllToUser(String username) {
    return emailRepository
        .findAll()
        .stream()
        .filter(email -> email.getToUser().equals(username))
        .collect(Collectors.toList());
  }

    @Override
    public List<Email> getAllFromUser(String username) {
        return emailRepository
                .findAll()
                .stream()
                .filter(email -> email.getFromUser().equals(username))
                .collect(Collectors.toList());
    }

  @Override
  public List<Email> getAll() {
    return emailRepository.findAll();
  }
}
