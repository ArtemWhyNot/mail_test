package com.Tsukanov.services;

import com.Tsukanov.entities.User;

public interface UserService {
    User loadUser(String username);
    User save(User user);

    boolean isAdmin(String username);
}
