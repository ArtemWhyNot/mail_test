package com.Tsukanov.services;

import com.Tsukanov.entities.Email;

import java.util.List;

public interface EmailService {
    Email send(Email email);

    List<Email> getAllToUser(String username);

    List<Email> getAllFromUser(String username);

    List<Email> getAll();
}
