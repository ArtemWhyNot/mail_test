package com.Tsukanov.controllers;

import com.Tsukanov.entities.User;
import com.Tsukanov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller("/login")
public class LoginController {

  @Autowired
  private UserService userService;

  @RequestMapping(method = RequestMethod.GET)
  public String home(Model model, HttpSession session) {
    User user = new User();
    model.addAttribute("user", user);
    return "login";
  }

  @RequestMapping(method = RequestMethod.POST)
  public String login(@ModelAttribute User user, Model model, HttpSession session) {
    System.out.println("LoginController.login - " + user.getUsername());
    System.out.println("LoginController.login" + userService.loadUser(user.getUsername()));
    User fromDB = userService.loadUser(user.getUsername());
    if(fromDB != null && fromDB.getPassword().equals(user.getPassword())) {
      session.setAttribute("username", user.getUsername());
      return "redirect:/";
    }
    model.addAttribute("user", new User());
    model.addAttribute("error", "wrong username or password");
    return "login";
  }
}
