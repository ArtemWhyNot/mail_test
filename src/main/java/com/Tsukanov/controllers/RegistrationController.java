package com.Tsukanov.controllers;

import com.Tsukanov.entities.User;
import com.Tsukanov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String getRegistrationPage(Model model) {
        model.addAttribute("user", new User());

        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@Valid @ModelAttribute User newUser, BindingResult bindingResult, HttpSession session, Model model) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (userService.loadUser(newUser.getUsername()) != null) {
            model.addAttribute("error", "username should be unique");
            return "registration";
        }
        User userToSave = userService.save(newUser);
        session.setAttribute("username", userToSave.getUsername());
        return "redirect:/";
    }
}
