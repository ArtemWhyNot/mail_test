package com.Tsukanov.controllers;

import com.Tsukanov.entities.Email;
import com.Tsukanov.services.EmailService;
import com.Tsukanov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String emailList(HttpSession session, Model model) {
    System.out.println("EmailController.emailList");
    String username = (String)session.getAttribute("username");
        if (username == null) {
            return "redirect:/login";
        }
        model.addAttribute("email", new Email());
        List<Email> emailsToUser = emailService.getAllToUser(username);
        List<Email> emailsFromUser = emailService.getAllFromUser(username);
        model.addAttribute("emailsToYou", emailsToUser);
        model.addAttribute("emailsFromYou", emailsFromUser);
        model.addAttribute("username", username);
        model.addAttribute("isAdmin", userService.isAdmin(username));
        return "emails";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String sendEmail(@ModelAttribute Email email, Model model, HttpSession session) {
        email.setFromUser((String)session.getAttribute("username"));
        if (userService.loadUser(email.getToUser()) == null) {
            model.addAttribute("error", "user doesn't exist");
            return emailList(session, model);
        }
        emailService.send(email);
        model.addAttribute("message", "email successfully passed");
        return emailList(session, model);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/emails/all")
    public String getAllEmails(HttpSession session, Model model) {
        String username = (String)session.getAttribute("username");
        if (username != null && !userService.isAdmin(username)) {
            return "/login";
        }

        model.addAttribute("allemails", emailService.getAll());
        model.addAttribute("username", username);

        return "allemails";
    }
}
